<h1>Questões de Lógica de Programação</h1>
<h2>Questões Fáceis</h2>
<ol>
    <li>Soma de dois números: Escreva um programa que recebe dois números como entrada e retorna a soma deles.</li>
    <li>Média de três números: Escreva um programa que calcula a média de três números fornecidos como entrada.</li>
    <li>Verificação de número par ou ímpar: Crie um programa que verifica se um número fornecido pelo usuário é par ou ímpar.</li>
    <li>Cálculo de fatorial: Escreva um programa que calcula o fatorial de um número fornecido pelo usuário.</li>
    <li>Identificação do maior de dois números: Escreva um programa que determina qual dos dois números fornecidos pelo usuário é o maior.</li>
    <li>Contagem regressiva: Escreva um programa que imprime uma contagem regressiva de 10 até 1.</li>
    <li>Verificação de palíndromo: Crie um programa que verifica se uma palavra fornecida pelo usuário é um palíndromo (ou seja, se ela é a mesma se lida de trás para frente).</li>
    <li>Conversão de temperatura: Escreva um programa que converte uma temperatura fornecida em graus Celsius para Fahrenheit.</li>
    <li>Identificação do maior de três números: Escreva um programa que determina qual dos três números fornecidos pelo usuário é o maior.</li>
    <li>Tabuada de multiplicação: Crie um programa que imprime a tabuada de multiplicação de um número fornecido pelo usuário.</li>
    <li>Verificação de número primo: Escreva um programa que verifica se um número fornecido pelo usuário é primo.</li>
    <li>Cálculo de média ponderada: Crie um programa que calcula a média ponderada de três notas, onde os pesos são fornecidos pelo usuário.</li>
    <li>Cálculo de juros simples: Escreva um programa que calcula o montante final após aplicar juros simples a um principal, com taxa de juros e período fornecidos pelo usuário.</li>
    <li>Contagem de vogais em uma frase: Crie um programa que conta quantas vogais há em uma frase fornecida pelo usuário.</li>
    <li>Verificação de número perfeito: Escreva um programa que verifica se um número fornecido pelo usuário é um número perfeito.</li>
    <li>Conversão de número binário para decimal: Crie um programa que converte um número binário fornecido pelo usuário para decimal.</li>
    <li>Identificação de números primos em um intervalo: Escreva um programa que identifica e imprime todos os números primos em um intervalo fornecido pelo usuário.</li>
    <li>Contagem de ocorrências de um caractere em uma string: Crie um programa que conta quantas vezes um caractere específico ocorre em uma string fornecida pelo usuário.</li>
    <li>Cálculo do volume de uma esfera: Escreva um programa que calcula o volume de uma esfera, dado o raio fornecido pelo usuário.</li>
    <li>Verificação de ano bissexto: Crie um programa que verifica se um ano fornecido pelo usuário é bissexto ou não.</li>
</ol>







<h2>Questões Médias</h2>
<ol>
    <li>Encontrar o número único: Dada uma lista de números onde todos os elementos ocorrem duas vezes, exceto um. Escreva um programa para encontrar o único número que ocorre uma vez.</li>
    <li>Verificação de equação balanceada: Escreva um programa que verifica se uma expressão matemática fornecida pelo usuário possui parênteses, colchetes e chaves balanceadas.</li>
    <li>Seleção de intervalo de datas: Escreva um programa que selecione todas as datas dentro de um intervalo especificado pelo usuário.</li>
    <li>Cálculo do determinante de uma matriz: Escreva um programa que calcule o determinante de uma matriz quadrada fornecida pelo usuário.</li>
    <li>Verificação de sequência Fibonacci: Implemente um programa que verifique se uma sequência de números é uma sequência de Fibonacci.</li>
    <li>Contagem de palavras em um texto: Escreva um programa que conte quantas vezes cada palavra aparece em um texto fornecido pelo usuário.</li>
    <li>Cálculo da raiz cúbica de um número: Escreva um programa que calcule a raiz cúbica de um número fornecido pelo usuário.</li>
    <li>Encontrar o segundo maior número: Dada uma lista de números, escreva um programa para encontrar o segundo maior número presente nessa lista.</li>
    <li>Verificação de ano bissexto: Crie um programa que verifica se um ano fornecido pelo usuário é bissexto ou não.</li>
    <li>Contagem de caracteres únicos em uma string: Escreva um programa que conta quantos caracteres únicos existem em uma string fornecida pelo usuário.</li>
    <li>Verificação de Triângulo: Crie um programa que verifique se três números fornecidos pelo usuário podem formar um triângulo válido.</li>
    <li>Cálculo do Máximo Comum Divisor (MDC): Escreva um programa que calcule o MDC de dois números fornecidos pelo usuário.</li>
    <li>Verificação de quadrado perfeito: Implemente um programa que verifica se um número fornecido pelo usuário é um quadrado perfeito.</li>
    <li>Leitor de string: Escreva um programa que leia uma string e imprima cada caractere separadamente, sem usar listas ou arrays.</li>
    <li>Contagem regressiva em intervalo personalizado: Escreva um programa que imprime uma contagem regressiva a partir de um número fornecido pelo usuário até 1.</li>
    <li>Identificação de Números Amigos: Implemente um programa que verifique se dois números fornecidos pelo usuário são "amigos" (a soma dos divisores de um número é igual ao outro número).</li>
    <li>Geração de Números Primos: Implemente um programa que gere uma lista de números primos até um determinado limite fornecido pelo usuário.</li>
    <li>Identificação de Números Pares em uma Lista: Escreva um programa que identifique e imprima todos os números pares em uma lista fornecida pelo usuário.</li>
    <li>Cálculo da Média de Idades: Escreva um programa que calcule a média das idades de um grupo de pessoas fornecidas pelo usuário.</li>
    <li>Identificação de Duplicatas em uma Lista: Crie um programa que identifique e imprima as duplicatas em uma lista de números fornecida pelo usuário.</li>
    <li>Identificação de Sequência Aritmética: Implemente um programa que verifique se uma lista de números fornecida pelo usuário forma uma sequência aritmética.</li>
</ol>







<h2>Questões Difíceis</h2>
<ol>
     <li>Problema dos Pares de Soma: Escreva um programa que, dada uma lista de números e um valor alvo, encontre todos os pares de números na lista que somam o valor alvo.</li>
    <li>Problema da Mochila 0-1: Dado um conjunto de itens, cada um com um peso e um valor, determine a quantidade máxima de valor que pode ser carregada em uma mochila com capacidade limitada.</li>
    <li>Algoritmo de Ordenação Quicksort: Implemente o algoritmo de ordenação Quicksort para ordenar uma lista de números fornecida pelo usuário.</li>
    <li>Verificação de Equação Quadrática: Crie um programa que verifique se uma equação quadrática fornecida pelo usuário é válida e, se for, calcule suas raízes.</li>
    <li>Decomposição em Fatores Primos: Escreva um programa que realize a decomposição de um número fornecido pelo usuário em seus fatores primos.</li>
    <li>Espaço da memória: Implemente um programa que simule o funcionamento da estratégia de alocação de memória "First-Fit" em um sistema operacional.</li>
    <li>Conversão de Número em Palavras: Escreva um programa que converta um número inteiro fornecido pelo usuário em palavras.</li>
    <li>Cálculo da Mediana de uma Lista: Implemente um programa que calcule a mediana de uma lista de números fornecida pelo usuário.</li>
    <li>Identificação de Sequência de Fibonacci: Escreva um programa que verifique se uma sequência de números é uma sequência de Fibonacci.</li>
    <li>Geração de Anagramas de uma Palavra: Crie um programa que gere todos os anagramas possíveis de uma palavra fornecida pelo usuário.</li>
    <li>Identificação de Sequência de Palavras Palindrômica: Escreva um programa que identifique se uma sequência de palavras fornecida pelo usuário é palindrômica.</li>
    <li>Validação de Número de Cartão de Crédito: Crie um programa que valide se um número de cartão de crédito fornecido pelo usuário é válido.</li>
    <li>Cálculo do Número de Primos Menores que N: Escreva um programa que calcule o número de números primos menores que um número N fornecido pelo usuário.</li>
    <li>Conversão de Datas entre Formatos: Crie um programa que converta uma data fornecida pelo usuário de um formato para outro, como de "DD/MM/AAAA" para "AAAA-MM-DD".</li>
    <li>Ordenação de Strings: Implemente um programa que ordene uma lista de strings fornecida pelo usuário em ordem alfabética.</li>
    <li>Conversão de Números Romanos para Inteiros: Crie um programa que converta um número romano fornecido pelo usuário em um número inteiro.</li>
    <li>Validação de Endereço de E-mail: Crie um programa que valide se um endereço de e-mail fornecido pelo usuário é válido.</li>
    <li>Problema do Bolsa de Valores: Implemente um algoritmo que ajuda os investidores a maximizar seus lucros na bolsa de valores.</li>
    <li>Identificação de Números de Harshad: Escreva um programa que identifique se um número fornecido pelo usuário é um número de Harshad.</li>
    <li>Geração de Números de Primos Entre Dois Limites: Escreva um programa que gere todos os números primos dentro de um intervalo fornecido pelo usuário.</li>
</ol>

</body>
</html>